;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'elpa-to-ebuilds
  authors "Dima Akater"
  first-publication-year-as-string "2020"
  org-files-in-order '("elpa-to-ebuilds-core"
                       "elpa-to-ebuilds-search-and-create"
                       "elpa-to-ebuilds")
  org-files-for-testing-in-order '("elpa-to-ebuilds-tests")
  site-lisp-config-prefix "50"
  license "GPL-3")
